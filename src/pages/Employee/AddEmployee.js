import React from "react";
import Header from "../../components/Header";
import Menu from "../../components/Menu";
import Footer from "../../components/Footer";
import Add from "./components/ContentAdd";

export default class AddEmployee extends React.Component {
  render() {
    return (
      <div>
        <Header name='Tambah Pegawai' />
        <Menu active='product' />
        <Add />
        <Footer />
      </div>
    );
  }
}
