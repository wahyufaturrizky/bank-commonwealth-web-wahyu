// First we need to import axios.js
import axios from "axios";
import { Auth } from "./auth";

import { URL_STG, URL_PROD } from "../config";

// const mode = "production";
const mode = "dev";

console.log("URL_STG", URL_STG);

// Next we make an 'instance' of it
const instance = axios.create({
  // .. where we make our configurations
  baseURL:
    mode === "dev" ? "http://dummy.restapiexample.com/api/v1/" : URL_PROD,
});

// Where you would set stuff like your 'Authorization' header, etc ...
// instance.defaults.headers.common["Authorization"] = `Bearer ${Auth()}`;

// Also add/ configure interceptors && all the other cool stuff

export default instance;
