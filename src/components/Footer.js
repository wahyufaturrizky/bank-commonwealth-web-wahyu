import React from "react";

export default function Footer() {
  return (
    /* Main Footer */
    <footer className='main-footer'>
      <strong>
        Copyright © 2014-2020{" "}
        <a href='https://gitlab.com/wahyufaturrizky'>wahyu fatur rizki</a>.
      </strong>
      All rights reserved.
      <div className='float-right d-none d-sm-inline-block'>
        <b>Version</b> 1.0.0
      </div>
    </footer>
  );
}
