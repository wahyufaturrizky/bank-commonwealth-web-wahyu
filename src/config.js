const dotenv = require("dotenv");
dotenv.config();
module.exports = {
  URL_STG: process.env.TEST_URL_STG,
  URL_PROD: process.env.TEST_URL_PROD,
};
